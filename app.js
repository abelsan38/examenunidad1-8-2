

const res = document.getElementById('res')
const res2 = document.getElementById('res2')
const errores = document.getElementById('error')
document.getElementById('submit').addEventListener('submit', (e) => {
    e.preventDefault();
    const valor = document.querySelector('input[name="as"]:checked').value;
    console.log(valor)
    mostrarDatos(valor);
})
async function mostrarDatos(datos) {
    url = `https://www.thecocktaildb.com/api/json/v1/1/filter.php?a=${datos}`
    try {
        const respuesta = await fetch(url)
        const resultado = await respuesta.json()
        imprimirDatos(resultado);
    } catch (error) {
        const h11 = document.createElement('h1')
        h11.innerText = 'Error al momento de buscar los datos';
        h11.classList.add('text-center', 'text-red-400')
        errores.appendChild(h11);
        console.log(error)
    }
}
function imprimirDatos(datos) {
    eliminar2()
    eliminar()
    const longitud = datos.drinks.length;
    const h2 = document.createElement('h2')
    if (longitud === 58) {
        h2.innerText = `Total de bebidas no alcoholicas es: ${longitud}`
        h2.classList.add('text-center', 'text-gray-500', 'font-bold', 'text-xl')
        res2.appendChild(h2)
        console.log(longitud)
    } else {
        h2.innerText = `Total de bebidas alcoholicas es: ${longitud}`
        h2.classList.add('text-center', 'text-gray-500', 'font-bold', 'text-xl')
        res2.appendChild(h2)
        console.log(longitud)
    }


    datos.drinks.forEach(dato => {
        const { strDrink, strDrinkThumb } = dato
        const div = document.createElement('div');
        const h1 = document.createElement('h1')
        h1.innerText = strDrink
        h1.classList.add('text-xl', 'font-bold', 'text-gray-500', 'my-2')
        res.appendChild(h1)
        const img = document.createElement('img')
        img.src = strDrinkThumb

        div.appendChild(img)
        div.appendChild(h1)
        res.appendChild(div)
    });

}

function eliminar() {
    while (res.firstChild) {
        res.removeChild(res.firstChild)
    }
}
function eliminar2() {
    while (res2.firstChild) {
        res2.removeChild(res2.firstChild)
    }
}

document.getElementById('limpiar').addEventListener('click', () => {
    eliminar()
    eliminar2()
})